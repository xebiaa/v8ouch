'use strict';

var vm = require('vm');

var interpret = function(text, sandbox) {
    return vm.runInNewContext("(" + text + ")", sandbox);
}

interpret.sandbox = function(obj) {
    return vm.createContext(obj);
}

module.exports = interpret;
