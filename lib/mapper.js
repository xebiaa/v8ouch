'use strict';

var interpret = require('./interpret');

var output = [];

var sandboxContext = interpret.sandbox({
    emit: function(key, value) {
        output.push([key, value]);
    }
});

var mappers = [];

module.exports = {
    reset: function() {
        mappers = [];
        // Available if node invoked with --expose_gc.
        if (global.gc) {
            global.gc();
        }
        return true;
    },
    add_fun: function(fun_str) {
        try {
            mappers.push(interpret(fun_str, sandboxContext));
            return true;
        } catch (e) {
            return { "error": "Can't add function", "reason": e.toString() };
        }
    },
    map_doc: function(doc) {
        sandboxContext._doc = doc;
        try {
            for (var i = 0; i < mappers.length; i++) {
                mappers[i](doc);
            }
            return [ output ];
        } finally {
            output = [];
        }
    }
}
