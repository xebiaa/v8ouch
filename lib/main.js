'use strict';

// Readline works fine in Node 0.8, but is broken in Node 0.6
var readline = require('readline');

var mapper = require('./mapper');
var reducer = require('./reducer');

var dispatchTable = {
    "reset": mapper.reset,
    "add_fun": mapper.add_fun,
    "map_doc": mapper.map_doc,
    "reduce": reducer.reduce,
    "rereduce": reducer.rereduce
}

var dispatcher = function(cmd, arg1, arg2) {
    var func = dispatchTable[cmd];
    return func(arg1, arg2);
}

var repl = function() {
    var stdin = process.openStdin();
    var stdout = process.stdout;
    var repl = readline.createInterface(stdin, stdout, null);

    // Are we dealing with a tty or a pipe here?
    var istty = stdin.isRaw;

    if (istty) {
      repl.setPrompt('> ');
      repl.prompt();
    }

    repl.on('line', function(line) {
        var cmd = JSON.parse(line);
        var out = dispatcher.apply(this, cmd);
        stdout.write(JSON.stringify(out));
        stdout.write("\n");
        istty && repl.prompt();
    }).on('close', function() {
        stdout.write('["log", "Have a great day!"]\n');
        process.exit(0);
    });
}

module.exports.dispatcher = dispatcher;
module.exports.repl = repl;
