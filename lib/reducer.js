'use strict';

var _ = require('underscore')._,
    vm = require('vm');

var reducerGlobals = {
    sum: function(values) {
        var total = 0;
        for (var i = 0, len = values.length; i < len; ++i) {
            total += values[i];
        }
        return total;
    }
};

var compileReducer = function(code) {
    var script = vm.createScript('(' + code + ')', '<reduce>');
    script.invoke = function(keys, values, rereduce) {
        var reducer = this.runInNewContext(reducerGlobals);
        return reducer(keys, values, rereduce);
    }
    return script;
}

var executeReducers = function(reducers, keys, values, rereduce) {
    var reducerCount = reducers.length;
    var reductions = new Array(reducerCount);
    for (var i = 0; i < reducerCount; ++i) {
        var reducer = reducers[i],
            script = compileReducer(reducer),
            reduction = script.invoke(keys, values, rereduce);
        reductions[i] = reduction;
    }
    return reductions;
};

module.exports = {
    reduce: function(reducers, keys_and_values) {
        var transposed = _.zip.apply(null, keys_and_values),
            keys = transposed[0],
            values = transposed[1],
            reductions = executeReducers(reducers, keys, values, false);
        return [true, reductions];
    },
    rereduce: function(rereducers, values) {
        var reductions = executeReducers(rereducers, null, values, true);
        return [true, reductions];
    }
};
