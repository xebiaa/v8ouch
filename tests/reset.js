'use strict';

var testCase = require('nodeunit').testCase;
var main = require('../lib/main.js').dispatcher;

module.exports = testCase({
    "Basic Reset": function(test) {
        var cmdResult = main('reset');
        test.strictEqual(cmdResult,true,'Reset should return true');
        test.done();
    }
});
