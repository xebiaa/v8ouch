'use strict';

var testCase = require('nodeunit').testCase
var server = require('../lib/main.js').dispatcher

module.exports = testCase({
    "reset": function(test) {
        var cmdResult = server('reset');
        test.strictEqual(cmdResult,true,'Reset should return true');
        test.done();
    },

    "add_fun": testCase({
        setUp: function(callback) {
            server('reset');
            callback();
        },
        "valid_javascript": function(test) {
            var cmdResult = server('add_fun', "function(doc) { if(doc.score > 50) emit(null, {'player_name': doc.name}); }");
            test.strictEqual(cmdResult,true,'Function could not be added');
            test.done();
        },
        "invalid_javascript": function(test) {
            var cmdResult = server('add_fun', "something that doesn't compile");
            // The reasons don't matter. We need to allow anything.
            test.ok(typeof cmdResult === 'object'
                && typeof cmdResult.error !== 'undefined'
                && typeof cmdResult.reason !== 'undefined',
                "Functions that cannot be compiled should yield an error object with an error and reason attribute.");
            test.done();
        }
    }),

    "map_doc": testCase({
        setUp: function(callback) {
            server('reset');
            server('add_fun', "function(doc) { if(doc.score > 50) emit(null, {'player_name': doc.name}); }");
            callback();
        },
        "single_emit": function(test) {
            var cmdResult = server('map_doc',{"_id":"8877AFF9789988EE","_rev":"3-235256484","name":"John Smith","score": 60});

            test.deepEqual(cmdResult, [
                [
                    [null, {"player_name":"John Smith"}]
                ]
            ], "Expected mapper results not returned for document.");
            test.done();
        },
        "no_emit": function(test) {
            var cmdResult = server('map_doc',{"_id":"9590AEB4585637FE","_rev":"1-674684684","name":"Jane Parker","score": 43});

            test.deepEqual(cmdResult, [
                [
                    // Mapper emits nothing.
                ]
            ], "Empty mapper results not returned for document.");
            test.done();
        }
    }),

    "reduce": function(test) {
        var cmdResult = server('reduce',["function(k, v) { return sum(v); }"],[[[1,"699b524273605d5d3e9d4fd0ff2cb272"],10],[[2,"c081d0f69c13d2ce2050d684c7ba2843"],20],[[null,"foobar"],3]]);

        test.deepEqual(cmdResult,[true,[33]],"Reducer did not return expected results.");
        test.done();
    },

    "rereduce": function(test) {
        var cmdResult = server('rereduce',["function(k, v, r) { return sum(v); }"],[33,55,66]);

        test.deepEqual(cmdResult,[true,[154]],"Rereducer did not return expected results.");
        test.done();
    }
});
